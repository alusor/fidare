import { StackNavigator } from 'react-navigation';
import * as views from './Views';


const App = StackNavigator({
    inicio: { screen: views.Login },    
    principal: { screen: views.Main },
    registro: { screen: views.Register }
}, {
    navigationOptions: {
        headerStyle: {
            backgroundColor: '#FDFDFD',
            borderBottomColor: '#FDFDFD',
            shadowColor : '#5bc4ff',
            shadowOpacity: 0,
            shadowOffset: {
            height: 0,
            },
            shadowRadius: 0,
            elevation: 0
        }, 
        headerTitleStyle: {
            color: '#424E75',
            fontWeight: 'bold'
        },
        headerBackTitle: ' ',
        headerBackTitleStyle: {
            color: '#424E75',
        },
        headerTintColor: '#F95273'
    }
    
    
});


export default App;