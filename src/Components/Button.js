import React from 'react';
import { TouchableOpacity, Text } from 'react-native';

const Button = ({onPress, title}) => {
    return (
        <TouchableOpacity onPress={onPress} style={[styles.button, { }]}>
            <Text style={ styles.buttonText }>{title}</Text>
        </TouchableOpacity>
    );
};

const styles = {
    button: {
        backgroundColor: '#F95273',
        paddingVertical: 10,
        paddingHorizontal: 20,
        borderRadius: 25,
        marginHorizontal: 10,
        marginTop: 15,
        shadowColor : '#F95273',
        shadowOpacity: .5,
        shadowOffset: {
        height: 5,
        width: 0
        },
        shadowRadius: 5,
        elevation: 0,
    },
    buttonText: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 16,
        textAlign: 'center'
    }
};

export { Button };
