import React from 'react';
import { View, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const InputText = (props) => {
    const { container, input } = styles;
    const { name, placeholder } = props;
    return (
        <View style={container}>
            <View style={{ width: 24, alignContent: 'center' }}>
                <Icon name={name} size={24} color="#424E75" />
            </View>
            <TextInput  placeholder={placeholder} style={input}/>
        </View>
    );
};

const styles = {
    container: {
        borderRadius: 50,
        marginHorizontal: 10,
        marginTop: 15,
        backgroundColor: 'white',
        shadowColor : '#9a9a9a',
        shadowOpacity: .5,
        shadowOffset: {
        height: 5,
        width: 0
        },
        shadowRadius: 5,
        elevation: 0,
        flexDirection: 'row',
        paddingHorizontal: 15,
        alignItems: 'center'

    },
    input: {
        flex: 1,
        marginLeft: 15,
        marginVertical: 10,
        alignSelf: 'stretch',        

    }
};

export { InputText };