import React, { Component } from 'react';
import { View, Image, TouchableOpacity } from 'react-native';
import { Content, Text } from 'native-base';

const history = require('../Img/history.png');
const payment = require('../Img/payment.png');
const coins = require('../Img/coins.png');

class Main extends Component {
    static navigationOptions = {
        headerTitle: 'Bienvenido'
    };
    render() {
        return(
            <View style={styles.container}>
                <View style={styles.card}>
                    <Text style={styles.store}>{`Abarrotes "Minerva"`}</Text>
                    <Text style={styles.adress}>{`Av. Vallarta`}</Text>
                </View>
                <View style={styles.card}>
                    <View style={{ flexDirection: 'row', justifyContent:'center' }}>
                        <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', flex: 1, margin: 5}}>
                            <Image source={payment} />
                            <Text style={styles.adress}>Nuevo prestamo</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', flex: 1, margin: 5}}>
                            <Image source={history} />
                            <Text style={styles.adress}>Historial</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent:'center' }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1}}>
                            <Image source={coins} />
                        </View>
                        <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end', flex: 1}}>
                            <Text style={styles.store}>{`Cuentas cobradas`}</Text>
                            <Text style={styles.adress}>{`$2,500.ºº`}</Text>
                            <Text style={styles.store}>{`Cuentas por cobrar`}</Text>
                            <Text style={styles.adress}>{`$3,500.ºº`}</Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1,
        backgroundColor: '#FDFDFD',
    },
    card: {
        borderRadius: 15,
        marginHorizontal: 10,
        marginTop: 15,
        backgroundColor: 'white',
        shadowColor : '#9a9a9a',
        shadowOpacity: .5,
        shadowOffset: {
        height: 5,
        width: 0
        },
        paddingVertical: 15,
        paddingHorizontal: 10,
        alignContent: 'center',
        justifyContent: 'center'
    },
    store: {
        fontWeight: 'bold',
        color: '#F95273',
        fontSize: 16,
        marginBottom: 5
    },
    adress: {
        fontWeight: 'bold',
        color: '#979DB2',
        fontSize: 14,
    },
};

export { Main };