import React, {Component} from 'react';
import { Content, Text } from 'native-base';
import { View, TouchableOpacity, Image, ActivityIndicator } from 'react-native';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'react-native-fetch-blob';
import _ from 'lodash';

const img = require('../Img/selfie.png');

const API_KEY = '72c07738187047b3bbbfddccbab601a5';

const image_picker_options = {
    title: 'Toma una selfie.',
    takePhotoButtonTitle: 'Tomar una foto',
    chooseFromLibraryButtonTitle: 'Elegir de la galeria',
    storageOptions: {
      skipBackup: true,
      path: 'images'
    }
  };


class Login extends Component {
    static navigationOptions = {
        headerTitle: 'Inicio de sesión'
    };
    constructor(props) {
        super(props);
        this._chageScene = this.changeScene.bind(this);
        this.state = {
            has_photo: false,
            isLoading: false
        };
    }
    changeScene(scene) {
        this.props.navigation.navigate(scene);
    }

  _pickImage() {

   this.setState({
       face_data: null
   });

   ImagePicker.showImagePicker(image_picker_options, (response) => {

     if(response.error){
       alert('Error getting the image. Please try again.');
     }else{

       let source = {uri: response.uri};

       this.setState({
         photo_style: {
           position: 'relative',
           width: response.width,
           height: response.height
         },
         has_photo: true,
         photo: source,
         photo_data: response.data,
         isLoading: true
       });
       this._detectFaces();

     }
   });

 }
 _detectFaces() {

       RNFetchBlob.fetch('POST', 'https://southcentralus.api.cognitive.microsoft.com/face/v1.0/detect?returnFaceId=true&returnFaceAttributes=age,gender', {
           'Accept': 'application/json',
           'Content-Type': 'application/octet-stream',
           'Ocp-Apim-Subscription-Key': API_KEY
       }, this.state.photo_data)
       .then((res) => {
           console.log(res.json());
           return res.json();      
       })
       .then((json) => {
            console.log(json);
           if(json.length){
               this.setState({
                   face_data: json,
                   isLoading: false
               });
               this.props.navigation.navigate('principal');

           }else{
               alert("Intenta de nuevo, no pudimos detectar tu rostro.");
               this.setState({
                isLoading: false,
                has_photo: false
            });
           }

           return json;
       })
       .catch (function (error) {
           console.log(error);
           this.setState({
            isLoading: false,
            has_photo: false
        });
           alert('Sorry, the request failed. Please try again.' + JSON.stringify(error));
       });


     }
    render() {
        return (
            <View style={styles.pageContainer}>
                <View style={styles.full}>
                    { this.state.has_photo ? (
                        <View>
                            <Image style={{ width: 128, height: 128, margin: 10 }} source={this.state.photo} />
                        </View>
                    ): (
                        <Image style={{ width: 96, height: 96, margin: 10 }} source={img}/>
                    )}
                    {
                        this.state.isLoading? (
                            <ActivityIndicator size='large' color='#424E75' />
                        ) : null
                    }
                    <Text style={styles.title}>Una selfie para iniciar sesión</Text>
                </View>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('principal')} style={styles.button}>
                    <Text style={ styles.buttonText }>Tomar foto</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this._chageScene('registro')} style={[styles.button, { backgroundColor: '#424E75',  shadowColor : '#424E75',}]}>
                    <Text style={ styles.buttonText }>Registrarse</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = {
    title: {
        fontWeight: 'bold',
        color: '#424E75',
    },
    pageContainer: {
        backgroundColor: '#FDFDFD',
        flex: 1
    },
    full: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        backgroundColor: '#F95273',
        paddingVertical: 10,
        paddingHorizontal: 20,
        borderRadius: 25,
        marginHorizontal: 30,
        marginBottom: 15,
        shadowColor : '#F95273',
        shadowOpacity: .5,
        shadowOffset: {
        height: 5,
        width: 0
        },
        shadowRadius: 5,
        elevation: 0,
    },
    buttonText: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 16,
        textAlign: 'center'
    }

};

export { Login };
