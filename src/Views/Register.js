import React, { Component } from 'react';
import { View, Image } from 'react-native';
import { Content, Text } from 'native-base';
import { InputText, Button } from '../Components';

const img = require('../Img/selfie.png');


class Register extends Component {
    static navigationOptions = {
        headerTitle: 'Registro'
    };
    render() {
        return (
            <View style={styles.container}>
                <Content style={styles.content}>
                    <View style={{ justifyContent: 'center', flex: 1, alignItems: 'center', marginVertical: 30 }}>
                        <Image style={{ width: 96, height: 96, margin: 10 }} source={img}/>
                        <Text style={styles.text}>{`Tomate una selfie, sera tu identificador. \nToca aqui.`}</Text>
                    </View>
                    <InputText name='user' placeholder='Nombre' />
                    <InputText name='home' placeholder='Dirección' />
                    <InputText name='mobile-phone' placeholder='Número de contacto' />
                    <Button title="Comenzar" />
                </Content>
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1
    },
    content: {
        backgroundColor: '#FDFDFD',
    },
    card: {
        backgroundColor: 'white',
        width: 200,
        height: 200,
        shadowColor : '#5bc4ff',
        shadowOpacity: .5,
        shadowOffset: {
        height: 10,
        },
        shadowRadius: 10,
        elevation: 0
    },
    text: {
        fontWeight: 'bold',
        color: '#424E75',
        textAlign: 'center'
    }
};

export { Register };